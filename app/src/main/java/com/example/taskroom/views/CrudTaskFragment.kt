package com.example.taskroom.views

import android.os.Build
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.taskroom.databinding.FragmentCrudTaskBinding
import com.example.taskroom.model.RepoImpl
import com.example.taskroom.model.models.Task
import com.example.taskroom.viewmodels.CrudTaskViewModel
import com.example.taskroom.viewmodels.TaskListViewModel
import com.example.taskroom.viewmodels.factories.ViewModelFactoryCrudTask
import com.example.taskroom.viewmodels.factories.ViewModelFactoryTaskList
import java.time.Clock
import java.time.Instant

class CrudTaskFragment : Fragment() {

    private var _binding: FragmentCrudTaskBinding? = null
    private val binding: FragmentCrudTaskBinding get() = _binding!!

    val repoImpl by lazy {
        RepoImpl(requireContext())
    }

    val vm by viewModels<CrudTaskViewModel> {
        ViewModelFactoryCrudTask(repoImpl)
    }

    val args by navArgs<CrudTaskFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCrudTaskBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (args.taskEntry != null) {
            vm.title = args.taskEntry!!.title
            vm.description = args.taskEntry!!.description
            initViews()
        }
        initListeners()
    }

    private fun initViews() = with(binding) {
        taskTitle.text = SpannableStringBuilder(args.taskEntry?.title)
        taskDescription.text = SpannableStringBuilder(args.taskEntry?.description)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun initListeners() = with(binding) {

        if (args.taskEntry != null) {
            validateInputs()
            createTaskButton.text = "Edit Task"
            vm.continueButton.observe(viewLifecycleOwner) { onOrOff ->
                createTaskButton.isEnabled = onOrOff
            }
            taskTitle.addTextChangedListener {
                validateInputs()
                vm.title = it.toString()
            }
            taskDescription.addTextChangedListener {
                validateInputs()
                vm.description = it.toString()
            }
            createTaskButton.setOnClickListener {
                val clock: Clock = Clock.systemDefaultZone()
                val time = clock.instant().toString()
                val instantBefore = Instant.parse(time).toString()
                val task = Task(
                    id = args.taskEntry!!.id,
                    title = vm.title,
                    description = vm.description,
                    completed = false,
                    updated = instantBefore,
                    date = instantBefore
                )
                vm.editTaskEntry(task)
                findNavController().navigateUp()
            }
        }
        else {

            vm.continueButton.observe(viewLifecycleOwner) { onOrOff ->
                createTaskButton.text = "Add Task"
                createTaskButton.isEnabled = onOrOff
            }
            taskTitle.addTextChangedListener {
                vm.title = it.toString()
                validateInputs()
            }
            taskDescription.addTextChangedListener {
                vm.description = it.toString()
                validateInputs()
            }
            createTaskButton.setOnClickListener {
                val clock: Clock = Clock.systemDefaultZone()
                val time = clock.instant().toString()
                val instantBefore = Instant.parse(time).toString()
                val task = Task(
                    title = vm.title,
                    description = vm.description,
                    completed = false,
                    updated = instantBefore,
                    date = instantBefore
                )
                vm.addTaskEntry(task)
                findNavController().navigateUp()
            }

        }

    }

    private fun validateInputs() {
        if (vm.title.isNotEmpty() && vm.description.isNotEmpty()) {
            vm.setContinueButtonValue(true)
        }
        else {
            vm.setContinueButtonValue(false)
        }
    }

}