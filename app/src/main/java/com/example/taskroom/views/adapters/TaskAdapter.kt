package com.example.taskroom.views.adapters

import android.content.Context
import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.taskroom.OnSwipeTouchListener
import com.example.taskroom.databinding.TaskEntryBinding
import com.example.taskroom.model.models.Task

class TaskAdapter(val navigateToCrudTask: (task: Task) -> Unit, val deleteTaskEntry: (task: Task) -> Unit) : RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {

    lateinit var data: List<Task>

    val self = this

    class TaskViewHolder(
        val binding: TaskEntryBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun applyEntry(task: Task) {
            binding.taskNum.text = (adapterPosition + 1).toString()
            binding.viewTaskEntry.text = task.title
            binding.completedCheckbox.setOnClickListener {
                task.completed = !task.completed
                if (task.completed) {
                    binding.viewTaskEntry.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                }
                else {
                    binding.viewTaskEntry.paintFlags = 0
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val binding = TaskEntryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TaskViewHolder(binding).apply {
            binding.root.setOnTouchListener(object : OnSwipeTouchListener(binding.root.context) {
                override fun onSwipeRight() {
                    super.onSwipeRight()
                    binding.root.visibility = View.INVISIBLE
                    deleteTaskEntry(data[adapterPosition])
                }
                override fun onClick() {
                    super.onClick()
                    navigateToCrudTask(data[adapterPosition])
                }
            })
        }
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        val taskEntry = data[position]
        holder.applyEntry(taskEntry)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun applyEntries(tasks: List<Task>) {
        data = tasks
    }

}