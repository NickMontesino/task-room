package com.example.taskroom.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.taskroom.databinding.FragmentTaskListBinding
import com.example.taskroom.model.RepoImpl
import com.example.taskroom.model.models.Task
import com.example.taskroom.utils.Resource
import com.example.taskroom.viewmodels.TaskListViewModel
import com.example.taskroom.viewmodels.factories.ViewModelFactoryTaskList
import com.example.taskroom.views.adapters.TaskAdapter

class TaskListFragment : Fragment() {

    var _binding: FragmentTaskListBinding? = null
    val binding: FragmentTaskListBinding get() = _binding!!

    val repoImpl by lazy {
        RepoImpl(requireContext())
    }

    val vm by viewModels<TaskListViewModel> {
        ViewModelFactoryTaskList(repoImpl)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentTaskListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    override fun onResume() {
        super.onResume()
        vm.getAllTasks()
    }

    private fun initListeners() = with(binding) {
        addTaskButton.setOnClickListener {
            navigateToCrudTask(null)
        }
        rvTasks.layoutManager = LinearLayoutManager(context)
        vm.tasks.observe(viewLifecycleOwner) { viewState ->
            when(viewState) {
                is Resource.Error -> {
                    Toast.makeText(context, viewState.message, Toast.LENGTH_SHORT).show()
                }
                is Resource.Loading -> {

                }
                is Resource.Success -> {
                    rvTasks.adapter = TaskAdapter(::navigateToCrudTask, ::deleteTaskEntry).apply {
                        applyEntries(viewState.data)
                    }
                }
            }
        }
    }

    private fun navigateToCrudTask(task: Task?) {
        val action = TaskListFragmentDirections.actionTaskListFragmentToCrudTaskFragment(task)
        findNavController().navigate(action)
    }

    private fun deleteTaskEntry(task: Task) {
        vm.deleteTask(task)
    }

}