package com.example.taskroom.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.taskroom.model.RepoImpl
import com.example.taskroom.model.models.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CrudTaskViewModel(val repo: RepoImpl) : ViewModel() {

    var title: String = ""
    var description: String = ""

    private val _continueButton = MutableLiveData<Boolean>(false)
    val continueButton: LiveData<Boolean> get() = _continueButton

    fun setContinueButtonValue(flag: Boolean) {
        _continueButton.value = flag
    }

    fun addTaskEntry(task: Task) = viewModelScope.launch(Dispatchers.Main) {
        repo.addTask(task)
    }

    fun editTaskEntry(task: Task) = viewModelScope.launch(Dispatchers.Main) {
        repo.editTask(task)
    }

}