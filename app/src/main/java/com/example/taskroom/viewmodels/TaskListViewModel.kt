package com.example.taskroom.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.taskroom.model.RepoImpl
import com.example.taskroom.model.models.Task
import com.example.taskroom.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TaskListViewModel(val repo: RepoImpl) : ViewModel() {

    private var _tasks = MutableLiveData<Resource<List<Task>>>(Resource.Loading())
    val tasks: LiveData<Resource<List<Task>>> get() = _tasks

    fun getAllTasks() = viewModelScope.launch(Dispatchers.Main) {
        val response = repo.grabAllTasks()
        _tasks.value = response
    }

    fun deleteTask(task: Task) = viewModelScope.launch(Dispatchers.Main) {
        repo.deleteTask(task)
    }

}