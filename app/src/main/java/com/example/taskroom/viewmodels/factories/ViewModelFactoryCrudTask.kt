package com.example.taskroom.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.taskroom.model.RepoImpl
import com.example.taskroom.viewmodels.CrudTaskViewModel
import com.example.taskroom.views.CrudTaskFragment

class ViewModelFactoryCrudTask(
    private val repo: RepoImpl
    ) : ViewModelProvider.NewInstanceFactory() {

        override fun <T: ViewModel> create(modelClass: Class<T>): T {
            return CrudTaskViewModel(repo) as T
        }

}