package com.example.taskroom.model

import com.example.taskroom.model.models.Task
import com.example.taskroom.utils.Resource

interface Repo {
    suspend fun addTask(task: Task)
    suspend fun editTask(task: Task)
    suspend fun deleteTask(task: Task)
    suspend fun grabAllTasks(): Resource<List<Task>>
}