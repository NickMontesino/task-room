package com.example.taskroom.model

import android.content.Context
import com.example.taskroom.model.models.Task
import com.example.taskroom.model.room.TaskDatabase
import com.example.taskroom.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class RepoImpl(context: Context): Repo {

    private val taskDAO = TaskDatabase.getDatabaseInstance(context).taskDAO()

    override suspend fun addTask(task: Task) = withContext(Dispatchers.IO) {
        taskDAO.addTaskEntry(task)
    }

    override suspend fun editTask(task: Task) = withContext(Dispatchers.IO) {
        taskDAO.addTaskEntry(task)
    }

    override suspend fun deleteTask(task: Task) {
        taskDAO.deleteTask(task)
    }

    override suspend fun grabAllTasks(): Resource<List<Task>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = taskDAO.grabAllTasks()
            Resource.Success(response)
        }
        catch (e: Exception) {
            Resource.Error(e.localizedMessage ?: "Something went wrong")
        }
    }

}