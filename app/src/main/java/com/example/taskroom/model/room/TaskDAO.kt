package com.example.taskroom.model.room

import androidx.room.*
import com.example.taskroom.model.models.Task

@Dao
interface TaskDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addTaskEntry(task: Task)

    @Query("SELECT * FROM task_entry")
    suspend fun grabAllTasks(): List<Task>

    @Delete
    suspend fun deleteTask(task: Task)

}