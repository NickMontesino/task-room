package com.example.taskroom.model.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.taskroom.model.models.Task

@Database(entities = [Task::class], exportSchema = false, version = 1)
abstract class TaskDatabase : RoomDatabase() {

    abstract fun taskDAO(): TaskDAO

    companion object {

        const val DB_NAME = "TASK_DB"

        fun getDatabaseInstance(context: Context): TaskDatabase {
            return Room
                .databaseBuilder(context, TaskDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }

    }

}